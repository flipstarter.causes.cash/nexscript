/*   Compiler.test.ts
 *
 * - This file is used to test the overall functioning of the compiler.
 * - It tests successful compilation using fixture .cash files in ../valid-contract-files.
 * - It tests compile errors using fixture .cash files in respective Error directories.
 */

import { URL } from 'url';
import { getSubdirectories, readCashFiles } from '../test-utils.js';
import * as Errors from '../../src/Errors.js';
import { compileString } from '../../src/index.js';

describe('Compiler', () => {
  describe('Successful compilation', () => {
    readCashFiles(new URL('../valid-contract-files', import.meta.url)).forEach((file) => {
      it(`${file.fn} should succeed`, () => {
        expect(() => compileString(file.contents)).not.toThrow();
      });
    });
  });

  describe('Compilation errors', () => {
    const errorTypes = getSubdirectories(new URL('.', import.meta.url));

    errorTypes.forEach((errorType) => {
      describe(errorType.toString(), () => {
        readCashFiles(new URL(errorType, import.meta.url)).forEach((file) => {
          it(`${file.fn} should throw ${errorType}`, () => {
            // Retrieve the correct Error constructor from the Errors.ts file
            const expectedError = Errors[errorType as keyof typeof Errors];

            if (!expectedError) throw new Error(`Invalid test configuration: error ${errorType} does not exist`);

            expect(() => compileString(file.contents)).toThrow(expectedError);
          });
        });
      });
    });
  });

//   it('Mcp Compilation', () => {
//     const source = `
// pragma nexscript >=0.6.0;

// mcpContract Test(int visible unused a, int visible b, int c) {

//   contract A(int visible unused a, int visible unused b, int visible unused c) {
//       function Av(string vA, int x) {
//           require(vA == "A");
//           require(x == 1);
//       }

//       function Bv(string vA, int x) {
//         require(vA == "B");
//         require(x == 2);
//       }
//   }

//   contract B() {
//     function Bv(string vB) {
//         require(vB == "B");
//     }
//   }

// }
// `;

//     const result = compileMcpString(source);
//     expect(result.contractName).toBe('Test');
//     expect(result.constructorInputs[0]).toMatchObject({
//       name: 'a',
//       type: 'int',
//       unused: true,
//       visible: true,
//     });

//     expect(result.constructorInputs[1]).toMatchObject({
//       name: 'b',
//       type: 'int',
//       unused: false,
//       visible: true,
//     });

//     expect(result.constructorInputs[2]).toMatchObject({
//       name: 'c',
//       type: 'int',
//       unused: false,
//       visible: false,
//     });
//   });
});
