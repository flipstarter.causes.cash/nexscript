import { ContractArtifact, Script, scriptToAsm } from '@nexscript/utils';
import { ContractNode } from '../ast/AST.js';

export function generateArtifact(contract: ContractNode, script: Script, dependencies: string[]): ContractArtifact {
  const constructorInputs = contract.parameters
    .map((parameter) => ({
      name: parameter.name,
      type: parameter.type.toString(),
      visible: parameter.visible,
      unused: parameter.unused,
    }));

  const abi = contract.functions.map((func) => ({
    name: func.name,
    inputs: func.parameters.map((parameter) => ({
      name: parameter.name,
      type: parameter.type.toString(),
    })),
  }));

  const bytecode = scriptToAsm(script);

  return {
    contractName: contract.name,
    constructorInputs,
    abi,
    dependencies,
    bytecode,
    contracts: [],
  };
}
