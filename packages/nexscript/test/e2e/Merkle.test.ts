import { SourceArtifact, SerializeProof, GetMerkleParams, MerkleHash160 } from '@nexscript/utils';
import { compileString } from '@nexscript/nexc';
import {
  Contract,
  ElectrumNetworkProvider} from '../../src/index.js';
import { fund } from '../fixture/vars.js';
import { MerkleTree } from "merkletreejs";

describe('Multiplex contract tests', () => {
  beforeAll(async () => {
  });

  it('verify op_exec inverts data', async () => {
    const artifact: SourceArtifact = {
      "contracts": [{
      "contractName": "P2Palindrome",
      "constructorInputs": [],
      "abi": [
        {
          "name": "spend",
          "inputs": [
          ]
        }
      ],
      // emulate OP_CONVERT, push as <<A><B>>, exec, resulting stack has <B> <A>
      "bytecode": "142e32be610a14d519bfcc9a561dc5f3da37a7a1f2148e89482338682c05769d317183390868c2b66bdb OP_0 OP_2 OP_EXEC 8e89482338682c05769d317183390868c2b66bdb OP_EQUALVERIFY 2e32be610a14d519bfcc9a561dc5f3da37a7a1f2 OP_EQUALVERIFY",
      "contracts": []}],
      "source": "",
      "compiler": {
        "name": "nexc",
        "version": "0.1.0"
      },
      "updatedAt": "2023-08-02T16:45:23.310Z"
    }
    const provider = new ElectrumNetworkProvider();
    const contract = new Contract(artifact, [], { provider });
    await fund(contract.address, 100000);
    await contract.functions.spend().to(contract.address, 1000n).send();
  });

  describe('depth 1', () => {
    it('generateRoot plain', async () => {
      const leaves = [
        "a", "b"
      ];
      const hashes = leaves.map(val => MerkleHash160(val));

      let leafIndex: number = 1;

      let tree = new MerkleTree(hashes, MerkleHash160, {
        hashLeaves: false,
        duplicateOdd: true,
      });

      const proof = tree.getProof(hashes[leafIndex]);
      const root = tree.getRoot();

      const serializedProof = SerializeProof(proof);

      const artifact: SourceArtifact = {
        "contracts": [{
        "contractName": "P2Palindrome",
        "constructorInputs": [],
        "abi": [
          {
            "name": "spend",
            "inputs": [
            ]
          }
        ],
        bytecode: "",
        "contracts": []}],
        "source": "",
        "compiler": {
          "name": "nexc",
          "version": "0.1.0"
        },
        "updatedAt": "2023-08-02T16:45:23.310Z"
      }
      artifact.contracts[0].bytecode = `
${serializedProof} OP_0 OP_1 OP_EXEC

${leafIndex.toString(16)}

766b5297647c687ea9c17c6c52967c
OP_SWAP
OP_ROT

${hashes[leafIndex].toString("hex")}

OP_SWAP

OP_3 OP_3 OP_EXEC

OP_NIP OP_NIP

${root.toString('hex')} OP_EQUALVERIFY
`;

      const provider = new ElectrumNetworkProvider();
      const contract = new Contract(artifact, [], { provider });
      await fund(contract.address, 100000);
      await contract.functions.spend().to(contract.address, 1000n).send();
    });

    it('generateRoot macro', async () => {
      const leaves = [
        "a", "b"
      ];
      const hashes = leaves.map(val => MerkleHash160(val));

      let leafIndex: number = 1;

      let tree = new MerkleTree(hashes, MerkleHash160, {
        hashLeaves: false,
        duplicateOdd: true,
      });

      const proof = tree.getProof(hashes[leafIndex]);
      const root = tree.getRoot();

      const serializedProof = SerializeProof(proof);

      const contractSource = `
contract merkleDatabase(bytes20 root, int treeSize) {
  function append(bytes merkleProof, bytes20 lastLeaf) {
    // VALIDATE the existence of chosen leaf.
    bytes20 generatedRoot = merkleRoot(1, merkleProof, treeSize, lastLeaf);
    require(generatedRoot == root); //Validate that the merkle proof is valid.
  }
}
    `;

      const provider = new ElectrumNetworkProvider();
      const compiled = compileString(contractSource);
      const contract = new Contract(compiled, [root.toString('hex'), BigInt(leafIndex)], { provider });
      await fund(contract.address, 100000);
      await contract.functions.append(serializedProof, hashes[leafIndex].toString('hex')).to(contract.address, 1000n).send();
    });
  });

  describe('depth 2', () => {
    it('generateRoot plain', async () => {
      const leaves = [
        "a", "b", "c", "d"
      ];
      const hashes = leaves.map(val => MerkleHash160(val));

      let leafIndex: number = 3;

      let tree = new MerkleTree(hashes, MerkleHash160, {
        hashLeaves: false,
        duplicateOdd: true,
      });

      const proof = tree.getProof(hashes[leafIndex]);
      const root = tree.getRoot();

      const serializedProof = SerializeProof(proof);

      const artifact: SourceArtifact = {
        "contracts": [{
        "contractName": "P2Palindrome",
        "constructorInputs": [],
        "abi": [
          {
            "name": "spend",
            "inputs": [
            ]
          }
        ],
        bytecode: "",
        "contracts": []}],
        "source": "",
        "compiler": {
          "name": "nexc",
          "version": "0.1.0"
        },
        "updatedAt": "2023-08-02T16:45:23.310Z"
      }
      artifact.contracts[0].bytecode = `
${serializedProof} OP_0 OP_2 OP_EXEC

${leafIndex.toString(16)}

766b5297647c687ea9c17c6c52967c
OP_SWAP
OP_ROT

${hashes[leafIndex].toString("hex")}

OP_SWAP

OP_3 OP_3 OP_EXEC

OP_3 OP_ROLL OP_3 OP_3 OP_EXEC

OP_NIP OP_NIP

${root.toString('hex')} OP_EQUALVERIFY
  `;

      const provider = new ElectrumNetworkProvider();
      const contract = new Contract(artifact, [], { provider });
      await fund(contract.address, 100000);
      await contract.functions.spend().to(contract.address, 1000n).send();
    });

    it('generateRoot macro', async () => {
      const leaves = [
        "a", "b", "c"
      ];
      const hashes = leaves.map(val => MerkleHash160(val));

      let leafIndex: number = 2;

      const { root, serializedProof } = GetMerkleParams(hashes, leafIndex);

      const contractSource = `
contract merkleDatabase(bytes20 root, int treeSize) {
  function append(bytes merkleProof, bytes20 lastLeaf) {
    // VALIDATE the existence of chosen leaf.
    bytes20 generatedRoot = merkleRoot(2, merkleProof, treeSize, lastLeaf);
    require(generatedRoot == root); //Validate that the merkle proof is valid.
  }
}
    `;

      const provider = new ElectrumNetworkProvider();
      const compiled = compileString(contractSource);
      const contract = new Contract(compiled, [root, BigInt(leafIndex)], { provider });
      await fund(contract.address, 100000);
      await contract.functions.append(serializedProof, hashes[leafIndex].toString('hex')).to(contract.address, 1000n).send();
    });
  });

  describe('depth 3', () => {
    it('generateRoot plain', async () => {
      const leaves = [
        "a", "b", "c", "d", "e", "f", "g"
      ];
      const hashes = leaves.map(val => MerkleHash160(val));

      let leafIndex: number = 5;

      let tree = new MerkleTree(hashes, MerkleHash160, {
        hashLeaves: false,
        duplicateOdd: true,
      });

      const proof = tree.getProof(hashes[leafIndex]);
      const root = tree.getRoot();

      const serializedProof = SerializeProof(proof);

      const artifact: SourceArtifact = {
        "contracts": [{
        "contractName": "P2Palindrome",
        "constructorInputs": [],
        "abi": [
          {
            "name": "spend",
            "inputs": [
            ]
          }
        ],
        bytecode: "",
        "contracts": []}],
        "source": "",
        "compiler": {
          "name": "nexc",
          "version": "0.1.0"
        },
        "updatedAt": "2023-08-02T16:45:23.310Z"
      }
      artifact.contracts[0].bytecode = `
${serializedProof} OP_0 OP_3 OP_EXEC

${leafIndex.toString(16)}

766b5297647c687ea9c17c6c52967c
OP_SWAP
OP_ROT

${hashes[leafIndex].toString("hex")}

OP_SWAP

OP_3 OP_3 OP_EXEC

OP_3 OP_ROLL OP_3 OP_3 OP_EXEC

OP_3 OP_ROLL OP_3 OP_3 OP_EXEC

OP_NIP OP_NIP

${root.toString('hex')} OP_EQUALVERIFY
  `;

      const provider = new ElectrumNetworkProvider();
      const contract = new Contract(artifact, [], { provider });
      await fund(contract.address, 100000);
      await contract.functions.spend().to(contract.address, 1000n).send();
    });

    it('generateRoot macro', async () => {
      const leaves = [
        "a", "b", "c", "d", "e", "f", "g"
      ];
      const hashes = leaves.map(val => MerkleHash160(val));

      let leafIndex: number = 5;

      let tree = new MerkleTree(hashes, MerkleHash160, {
        hashLeaves: false,
        duplicateOdd: true,
      });

      const proof = tree.getProof(hashes[leafIndex]);
      const root = tree.getRoot();

      const serializedProof = SerializeProof(proof);

      const contractSource = `
contract merkleDatabase(bytes20 root, int treeSize) {
  function append(bytes merkleProof, bytes20 lastLeaf) {
    // VALIDATE the existence of chosen leaf.
    bytes20 generatedRoot = merkleRoot(3, merkleProof, treeSize, lastLeaf);
    require(generatedRoot == root); //Validate that the merkle proof is valid.
  }
}
    `;

      const provider = new ElectrumNetworkProvider();
      const compiled = compileString(contractSource);
      const contract = new Contract(compiled, [root.toString('hex'), BigInt(leafIndex)], { provider });
      await fund(contract.address, 100000);
      await contract.functions.append(serializedProof, hashes[leafIndex].toString('hex')).to(contract.address, 1000n).send();
    });
  });

  it('e2e merkle database', async () => {
    const source = /* js */`
// lastElement index is tree size - 1
contract merkleDatabase(bytes20 root, int lastElementIndex) {
  function append(bytes proof, bytes20 lastLeaf, bytes20 newLeaf) {

    // VALIDATE the existence of chosen leaf.
    bytes20 generatedRoot = merkleRoot(2, proof, lastElementIndex, lastLeaf ); // Generate a merkle root from the merkle proof.
    require(generatedRoot == root); //Validate that the merkle proof is valid.

    // APPEND the chosen leaf to the new leaf.
    lastElementIndex = lastElementIndex + 1; // Increment the leaf index.
    bytes20 generatedRootNew = merkleRoot(2, proof, lastElementIndex, newLeaf ); // Generate a merkle root for the new merkle leaf.

    // Apply changes to child UTXO
    bytes20 templateHash = hash160(this.activeBytecode); // Extract the template hash from the lockingbytecode
    bytes20 constraintHash = hash160(encodeData(generatedRootNew) + encodeNumber(lastElementIndex)); // Create the new constraintScript with the new root and new lastElementIndex.
    bytes newContractLock = new LockingBytecodeP2ST(templateHash, constraintHash, bytes(0x)); // Generate new locking byte code for the child UTXO.
    require(tx.outputs[0].lockingBytecode == newContractLock); // Lock the locking script for the child UTXO.
  }

  function update(bytes proof, int leafIndex, bytes20 leaf, bytes20 newLeaf) {

    // VALIDATE the existence of chosen leaf.
    bytes20 generatedRoot = merkleRoot(2, proof, leafIndex, leaf ); // Generate a merkle root from the merkle proof.
    require(generatedRoot == root); //Validate that the merkle proof is valid.

    // UPDATE the chosen leaf to the new leaf.
    bytes20 generatedRootNew = merkleRoot(2, proof, leafIndex, newLeaf ); // Generate a merkle root for the new merkle leaf.

    // Apply changes to child UTXO
    bytes20 templateHash = hash160(this.activeBytecode); // Extract the template hash from the lockingbytecode
    bytes20 constraintHash = hash160(encodeData(generatedRootNew) + encodeNumber(lastElementIndex)); // Create the new constraintScript with the new root.
    bytes newContractLock = new LockingBytecodeP2ST(templateHash, bytes(constraintHash), bytes(0x)); // Generate new locking byte code for the child UTXO.
    require(tx.outputs[0].lockingBytecode == newContractLock); // Lock the locking script for the child UTXO.
  }
}`;

    const leaves = [
      "a", "b", "c"
    ];
    const hashes = leaves.map(val => MerkleHash160(val));

    let leafIndex: number = 2;

    const { root, serializedProof } = GetMerkleParams(hashes, leafIndex);

    const artifact = compileString(source);
    const provider = new ElectrumNetworkProvider();

    // initial contract with 3 elements
    const contract3 = new Contract(artifact, [root, BigInt(leaves.length - 1)], { provider });
    await fund(contract3.address, 10000);

    // update "a" to "qq" at index 0
    const updatedLeaves = [
      "qq", "b", "c"
    ];
    const updatedHashes = updatedLeaves.map(val => MerkleHash160(val));

    let updatedLeafIndex: number = 0;

    const { root: updatedRoot, serializedProof: updatedSerializedProof } = GetMerkleParams(updatedHashes, updatedLeafIndex);

    // compute the next contract address by creating it
    const updatedContract = new Contract(artifact, [updatedRoot, BigInt(updatedLeaves.length - 1)], { provider });
    await contract3.functions.update(updatedSerializedProof, BigInt(updatedLeafIndex), MerkleHash160("a"), MerkleHash160("qq")).to(updatedContract.address, 8000n).withoutChange().send();
    expect(await updatedContract.getBalance()).toBeGreaterThanOrEqual(1000n);

    // insert new leaf "d" at index 3
    const newLeaves = [
      "qq", "b", "c", "d"
    ];
    const newHashes = newLeaves.map(val => MerkleHash160(val));

    let newLeafIndex: number = 3;

    const { root: newRoot } = GetMerkleParams(newHashes, newLeafIndex);

    // compute the next contract address by creating it
    const contract4 = new Contract(artifact, [newRoot, BigInt(newLeaves.length - 1)], { provider });

    const { serializedProof: updatedSerializedProofForLastElement } = GetMerkleParams(newHashes, newLeafIndex);

    await updatedContract.functions.append(updatedSerializedProofForLastElement, MerkleHash160("c"), MerkleHash160("d")).to(contract4.address, 6000n).withoutChange().send();
    expect(await contract4.getBalance()).toBeGreaterThanOrEqual(1000n);
  });
});
