import { SourceArtifact } from '@nexscript/utils';
import { Contract, ElectrumNetworkProvider } from '../../src/index.js';
import {
  fund,
} from '../fixture/vars.js';

describe('HodlVault', () => {
  beforeAll(async () => {
  });

  describe('send', () => {
    it('test constructor parameter ordering', async () => {
      // given
      const provider = new ElectrumNetworkProvider();

      const artifact = {
        contracts: [{
          contracts: [],
          contractName: 'test',
          constructorInputs: [
            {
              name: 'a', type: 'string', visible: false, unused: false,
            },
            {
              name: 'b', type: 'string', visible: false, unused: false,
            },
            {
              name: 'c', type: 'string', visible: false, unused: false,
            },
          ],
          abi: [{ name: 'test', inputs: [] }],
          bytecode: 'OP_FROMALTSTACK OP_FROMALTSTACK OP_FROMALTSTACK 61 OP_EQUALVERIFY 62 OP_EQUALVERIFY 63 OP_EQUALVERIFY',
        }],
        source: '\n'
          + '      contract test(string a, string b, string c) {\n'
          + '        function test() {\n'
          + '          require(a == "a");\n'
          + '          require(b == "b");\n'
          + '          require(c == "c");\n'
          + '        }\n'
          + '      }',
        compiler: { name: 'nexc', version: '0.3.0' },
        updatedAt: '2023-08-23T11:57:58.838Z',
      } as SourceArtifact;

      const contract = new Contract(artifact, ['a', 'b', 'c'], { provider });

      await fund(contract.address, 100000);
      await contract.functions
        .test()
        .to(contract.address, 10000n)
        .send();
    });

    it('test constructor visible parameter ordering', async () => {
      // given
      const provider = new ElectrumNetworkProvider();

      const artifact = {
        contracts: [{
          contracts: [],
          contractName: 'test',
          constructorInputs: [
            {
              name: 'a', type: 'string', visible: true, unused: false,
            },
            {
              name: 'b', type: 'string', visible: true, unused: false,
            },
            {
              name: 'c', type: 'string', visible: true, unused: false,
            },
          ],
          abi: [{ name: 'test', inputs: [] }],
          bytecode: 'OP_FROMALTSTACK OP_FROMALTSTACK OP_FROMALTSTACK 61 OP_EQUALVERIFY 62 OP_EQUALVERIFY 63 OP_EQUALVERIFY',
        }],
        source: '\n'
          + '      contract test(string visible a, string visible b, string visible c) {\n'
          + '        function test() {\n'
          + '          require(a == "a");\n'
          + '          require(b == "b");\n'
          + '          require(c == "c");\n'
          + '        }\n'
          + '      }',
        compiler: { name: 'nexc', version: '0.3.0' },
        updatedAt: '2023-08-23T11:58:06.009Z',
      };

      const contract = new Contract(artifact, ['a', 'b', 'c'], { provider });

      await fund(contract.address, 100000);
      await contract.functions
        .test()
        .to(contract.address, 10000n)
        .send();
    });

    it('test constructor mixed visibility parameter ordering', async () => {
      // given
      const provider = new ElectrumNetworkProvider();

      const artifact = {
        contracts: [{
          contracts: [],
          contractName: 'test',
          constructorInputs: [
            {
              name: 'a', type: 'string', visible: false, unused: false,
            },
            {
              name: 'b', type: 'string', visible: false, unused: false,
            },
            {
              name: 'c', type: 'string', visible: true, unused: false,
            },
            {
              name: 'd', type: 'string', visible: true, unused: false,
            },
          ],
          abi: [{ name: 'test', inputs: [] }],
          bytecode: 'OP_FROMALTSTACK OP_FROMALTSTACK OP_FROMALTSTACK OP_FROMALTSTACK 61 OP_EQUALVERIFY 62 OP_EQUALVERIFY 63 OP_EQUALVERIFY 64 OP_EQUALVERIFY',
        }],
        source: '\n'
          + '      contract test(string a, string b, string visible c, string visible d) {\n'
          + '        function test() {\n'
          + '          require(a == "a");\n'
          + '          require(b == "b");\n'
          + '          require(c == "c");\n'
          + '          require(d == "d");\n'
          + '        }\n'
          + '      }',
        compiler: { name: 'nexc', version: '0.3.0' },
        updatedAt: '2023-08-23T11:58:13.165Z',
      };

      const contract = new Contract(artifact, ['a', 'b', 'c', 'd'], { provider });

      await fund(contract.address, 100000);
      await contract.functions
        .test()
        .to(contract.address, 10000n)
        .send();
    });

    it('test constructor mixed visibility parameter ordering also unused', async () => {
      // given
      const provider = new ElectrumNetworkProvider();

      const artifact = {
        contracts: [{
          contracts: [],
          contractName: 'test',
          constructorInputs: [
            {
              name: 'a', type: 'string', visible: false, unused: false,
            },
            {
              name: 'b', type: 'string', visible: false, unused: false,
            },
            {
              name: 'c', type: 'string', visible: true, unused: false,
            },
            {
              name: 'd', type: 'string', visible: true, unused: false,
            },
            {
              name: 'e', type: 'string', visible: true, unused: true,
            },
            {
              name: 'f', type: 'string', visible: true, unused: true,
            },
          ],
          abi: [{ name: 'test', inputs: [] }],
          bytecode: 'OP_FROMALTSTACK OP_DROP OP_FROMALTSTACK OP_DROP OP_FROMALTSTACK OP_FROMALTSTACK OP_FROMALTSTACK OP_FROMALTSTACK 61 OP_EQUALVERIFY 62 OP_EQUALVERIFY 63 OP_EQUALVERIFY 64 OP_EQUALVERIFY',
        }],
        source: '\n'
          + '      contract test(string a, string b, string visible c, string visible d, string visible unused e, string visible unused f) {\n'
          + '        function test() {\n'
          + '          require(a == "a");\n'
          + '          require(b == "b");\n'
          + '          require(c == "c");\n'
          + '          require(d == "d");\n'
          + '        }\n'
          + '      }',
        compiler: { name: 'nexc', version: '0.3.0' },
        updatedAt: '2023-08-23T11:58:20.365Z',
      } as SourceArtifact;

      const contract = new Contract(artifact, ['a', 'b', 'c', 'd', 'e', 'f'], { provider });

      await fund(contract.address, 100000);
      await contract.functions
        .test()
        .to(contract.address, 10000n)
        .send();
    });

    it('test constructor mixed visibility parameter ordering also unused and satisfier arguments', async () => {
      // given
      const provider = new ElectrumNetworkProvider();

      const artifact = {
        contracts: [{
          contracts: [],
          contractName: 'test',
          constructorInputs: [
            {
              name: 'a', type: 'string', visible: false, unused: false,
            },
            {
              name: 'b', type: 'string', visible: false, unused: false,
            },
            {
              name: 'c', type: 'string', visible: true, unused: false,
            },
            {
              name: 'd', type: 'string', visible: true, unused: false,
            },
            {
              name: 'e', type: 'string', visible: true, unused: true,
            },
            {
              name: 'f', type: 'string', visible: true, unused: true,
            },
          ],
          abi: [{
            name: 'test',
            inputs: [
              { name: 'g', type: 'string' },
              { name: 'h', type: 'string' },
            ],
          },
          ],
          bytecode: 'OP_FROMALTSTACK OP_DROP OP_FROMALTSTACK OP_DROP OP_FROMALTSTACK OP_FROMALTSTACK OP_FROMALTSTACK OP_FROMALTSTACK OP_4 OP_ROLL 67 OP_EQUALVERIFY OP_4 OP_ROLL 68 OP_EQUALVERIFY 61 OP_EQUALVERIFY 62 OP_EQUALVERIFY 63 OP_EQUALVERIFY 64 OP_EQUALVERIFY',
        }],
        source: '\n'
          + '      contract test(string a, string b, string visible c, string visible d, string visible unused e, string visible unused f) {\n'
          + '        function test(string g, string h) {\n'
          + '          require(g == "g");\n'
          + '          require(h == "h");\n'
          + '\n'
          + '          require(a == "a");\n'
          + '          require(b == "b");\n'
          + '          require(c == "c");\n'
          + '          require(d == "d");\n'
          + '        }\n'
          + '      }',
        compiler: { name: 'nexc', version: '0.3.0' },
        updatedAt: '2023-08-23T11:58:27.539Z',
      } as SourceArtifact;

      const contract = new Contract(artifact, ['a', 'b', 'c', 'd', 'e', 'f'], { provider });

      await fund(contract.address, 100000);
      await contract.functions
        .test('g', 'h')
        .to(contract.address, 10000n)
        .send();
    });

    it('test satisfier argument ordering', async () => {
      // given
      const provider = new ElectrumNetworkProvider();

      const artifact = {
        contracts: [{
          contracts: [],
          contractName: 'test',
          constructorInputs: [],
          abi: [{
            name: 'test',
            inputs: [
              { name: 'a', type: 'string' },
              { name: 'b', type: 'string' },
              { name: 'c', type: 'string' },
            ],
          },
          ],
          bytecode: '61 OP_EQUALVERIFY 62 OP_EQUALVERIFY 63 OP_EQUALVERIFY',
        }],
        source: '\n'
          + '      contract test() {\n'
          + '        function test(string a, string b, string c) {\n'
          + '          require(a == "a");\n'
          + '          require(b == "b");\n'
          + '          require(c == "c");\n'
          + '        }\n'
          + '      }',
        compiler: { name: 'nexc', version: '0.3.0' },
        updatedAt: '2023-08-23T11:58:34.642Z',
      } as SourceArtifact;

      const contract = new Contract(artifact, [], { provider });

      await fund(contract.address, 100000);
      await contract.functions
        .test('a', 'b', 'c')
        .to(contract.address, 10000n)
        .send();
    });

    it('test satisfier argument ordering, 3 functions', async () => {
      const artifact = {
        contracts: [{
          contracts: [],
          contractName: 'test',
          constructorInputs: [
            {
              name: 'x',
              type: 'string',
              visible: false,
              unused: false,
            },
            {
              name: 'y',
              type: 'string',
              visible: false,
              unused: false,
            },
            {
              name: 'z',
              type: 'string',
              visible: false,
              unused: false,
            },
          ],
          abi: [
            {
              name: 'test',
              inputs: [
                {
                  name: 'a',
                  type: 'string',
                },
                {
                  name: 'b',
                  type: 'string',
                },
                {
                  name: 'c',
                  type: 'string',
                },
              ],
            },
            {
              name: 'test2',
              inputs: [
                {
                  name: 'd',
                  type: 'string',
                },
                {
                  name: 'e',
                  type: 'string',
                },
                {
                  name: 'f',
                  type: 'string',
                },
              ],
            },
            {
              name: 'test3',
              inputs: [],
            },
          ],
          bytecode: 'OP_FROMALTSTACK OP_FROMALTSTACK OP_FROMALTSTACK OP_3 OP_PICK OP_0 OP_NUMEQUAL OP_IF OP_4 OP_ROLL 61 OP_EQUALVERIFY OP_4 OP_ROLL 62 OP_EQUALVERIFY OP_4 OP_ROLL 63 OP_EQUALVERIFY OP_2DROP OP_2DROP OP_ELSE OP_3 OP_PICK OP_1 OP_NUMEQUAL OP_IF OP_4 OP_ROLL 64 OP_EQUALVERIFY OP_4 OP_ROLL 65 OP_EQUALVERIFY OP_4 OP_ROLL 66 OP_EQUALVERIFY OP_2DROP OP_2DROP OP_ELSE OP_3 OP_ROLL OP_2 OP_NUMEQUALVERIFY 78 OP_EQUALVERIFY 79 OP_EQUALVERIFY 7a OP_EQUALVERIFY OP_ENDIF OP_ENDIF',
        }],
        source: `
            contract test(string x, string y, string z) {
              function test(string a, string b, string c) {
                require(a == "a");
                require(b == "b");
                require(c == "c");
              }

              function test2(string d, string e, string f) {
                require(d == "d");
                require(e == "e");
                require(f == "f");
              }

              function test3() {
                require(x == "x");
                require(y == "y");
                require(z == "z");
              }
            }
      `,
        compiler: {
          name: 'nexc',
          version: '0.5.0',
        },
        updatedAt: '2023-09-15T18:45:39.497Z',
      } as SourceArtifact;

      // given
      const provider = new ElectrumNetworkProvider();

      const contract = new Contract(artifact, ['x', 'y', 'z'], { provider });

      await fund(contract.address, 100000);
      await contract.functions
        .test('a', 'b', 'c')
        .to(contract.address, 10000n)
        .send();
    });
  });
});
