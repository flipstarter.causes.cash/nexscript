import { Contract, SignatureTemplate, ElectrumNetworkProvider } from '../../../src/index.js';
import {
  alicePkh,
  bobPkh,
  aliceAddress,
  alicePub,
  alicePriv,
  fund,
} from '../../fixture/vars.js';
import { getTxOutputs } from '../../test-util.js';
import simpleCovenantArtifact from '../../fixture/old/simple_covenant.json' assert { type: 'json' };
import mecenasBorderArtifact from '../../fixture/old/mecenas_border.json' assert { type: 'json' };

describe.skip('v0.6.0 - Simple Covenant', () => {
  let covenant: Contract;

  beforeAll(async () => {
    const provider = new ElectrumNetworkProvider();
    covenant = new Contract(simpleCovenantArtifact, [], { provider });

    await fund(covenant.address, 10000);

    console.log(covenant.address);
  });

  describe('send', () => {
    it('should succeed', async () => {
      // given
      const to = covenant.address;
      const amount = 1000n;

      // when
      const tx = await covenant.functions
        .spend(alicePub, new SignatureTemplate(alicePriv))
        .to(to, amount)
        .send();

      // then
      const txOutputs = getTxOutputs(tx);
      expect(txOutputs).toEqual(expect.arrayContaining([{ to, amount }]));
    });
  });
});

describe.skip('v0.6.0 - Bytecode VarInt Border Mecenas', () => {
  let mecenas: Contract;
  const pledge = 10000n;

  beforeAll(async () => {
    const provider = new ElectrumNetworkProvider();
    mecenas = new Contract(mecenasBorderArtifact, [alicePkh, bobPkh, pledge], { provider });
    await fund(mecenas.address, 100000);
    console.log(mecenas.address);
  });

  it('should succeed when sending pledge to receiver', async () => {
    // given
    const to = aliceAddress;
    const amount = pledge;

    // when
    const tx = await mecenas.functions
      .receive(alicePub, new SignatureTemplate(alicePriv))
      .to(to, amount)
      .withHardcodedFee(1000n)
      .send();

    // then
    const txOutputs = getTxOutputs(tx);
    expect(txOutputs).toEqual(expect.arrayContaining([{ to, amount }]));
  });
});
