import { compileString } from '@nexscript/nexc';
import {
  ElectrumNetworkProvider,
  McpContract,
} from '../../src/index.js';
import {
  aliceAddress, fund,
} from '../fixture/vars.js';

describe('Mcp contract support', () => {
  beforeAll(async () => {
  });

  it('test mcp1', async () => {
    const source = `
pragma nexscript >=0.6.0;

contract Test() {

  contract A() {
    function A(string vA, int x) {
        require(vA == "A");
        require(x == 1);
    }

    function B(string vA, int x) {
      require(vA == "B");
      require(x == 2);
    }
  }

  contract B() {
    function Bv(string vB) {
        require(vB == "B");
    }
  }
  contract C() {
    function Cv(string vC) {
        require(vC == "C");
    }
  }
  contract D() {
    function Dv(string vD) {
        require(vD == "D");
    }
  }
  contract E() {
    function Ev(string vE) {
        require(vE == "E");
    }
  }
  contract F() {
    function Fv(string vF) {
        require(vF == "F");
    }
  }
  contract G() {
    function Gv(string vG) {
        require(vG == "G");
    }
  }
  contract H() {
    function Hv(string vH) {
        require(vH == "H");
    }
  }
  contract I() {
    function Iv(string vI) {
        require(vI == "I");
    }
  }
  contract J() {
    function Jv(string vJ) {
        require(vJ == "J");
    }
  }
  contract K() {
    function Kv(string vK) {
        require(vK == "K");
    }
  }
  contract L() {
    function Lv(string vL) {
        require(vL == "L");
    }
  }
  contract M() {
    function Mv(string vM) {
        require(vM == "M");
    }
  }
  contract N() {
    function Nv(string vN) {
        require(vN == "N");
    }
  }
  contract O() {
    function Ov(string vO) {
        require(vO == "O");
    }
  }
  contract P() {
    function Pv(string vP) {
        require(vP == "P");
    }
  }

  contract Q() {
    function Qv(string vQ) {
        require(vQ == "Q");
    }
  }
}
`;

    const mcpArtifact = compileString(source);

    const provider = new ElectrumNetworkProvider();
    const mcp = new McpContract(mcpArtifact.contracts[0], [], { provider });

    // 2-function MAST contract
    await fund(mcp.address, 10000);
    await mcp.execute({
      contractName: 'A',
      functionName: 'A',
      parameters: ['A', 1n],
    }).to(aliceAddress, 1000n).withoutChange().send();

    // single function MAST contract
    await fund(mcp.address, 10000);
    await mcp.execute({
      contractName: 'B',
      functionName: 'Bv',
      parameters: ['B'],
    }).to(aliceAddress, 1000n).withoutChange().send();

    // wrong argument
    await fund(mcp.address, 10000);
    await expect(mcp.execute({
      contractName: 'C',
      functionName: 'Cv',
      parameters: ['A'],
    }).to(aliceAddress, 1000n).withoutChange().send()).rejects.toThrow();

    // compact proof
    await fund(mcp.address, 10000);
    await mcp.execute({
      contractName: 'Q',
      functionName: 'Qv',
      parameters: ['Q'],
    }).to(aliceAddress, 1000n).withoutChange().send();
  });

  it('test mcp, constraint arguments', async () => {
    const source = `
pragma nexscript >=0.6.0;

contract Test(int a, int b, int c) {

  contract A(int visible unused a, int visible unused b, int visible c) {
    function A(string vA, int x) {
        require(c == 3);

        require(vA == "A");
        require(x == 1);
    }

    function B(string vA, int x) {
      require(vA == "B");
      require(x == 2);
    }
  }

  contract B(int visible unused a, int visible b, int visible c) {
    function Bv(string vB) {
        require(b == 2);
        require(c == 3);
        require(vB == "B");
    }
  }
  contract C(int visible a, int visible unused b, int visible c) {
    function Cv(string vC) {
        require(a == 1);
        require(c == 3);
        require(vC == "C");
    }
  }
  contract D() {
    function Dv(string vD) {
        require(vD == "D");
    }
  }
  contract E() {
    function Ev(string vE) {
        require(vE == "E");
    }
  }
  contract F() {
    function Fv(string vF) {
        require(vF == "F");
    }
  }
  contract G() {
    function Gv(string vG) {
        require(vG == "G");
    }
  }
  contract H() {
    function Hv(string vH) {
        require(vH == "H");
    }
  }
  contract I() {
    function Iv(string vI) {
        require(vI == "I");
    }
  }
  contract J() {
    function Jv(string vJ) {
        require(vJ == "J");
    }
  }
  contract K() {
    function Kv(string vK) {
        require(vK == "K");
    }
  }
  contract L() {
    function Lv(string vL) {
        require(vL == "L");
    }
  }
  contract M() {
    function Mv(string vM) {
        require(vM == "M");
    }
  }
  contract N() {
    function Nv(string vN) {
        require(vN == "N");
    }
  }
  contract O() {
    function Ov(string vO) {
        require(vO == "O");
    }
  }
  contract P() {
    function Pv(string vP) {
        require(vP == "P");
    }
  }

}
`;

    const mcpArtifact = compileString(source);

    const provider = new ElectrumNetworkProvider();
    const mcp = new McpContract(mcpArtifact.contracts[0], [1n, 2n, 3n], { provider });

    // 2-function MAST contract, right wrong visible unused ordering
    await fund(mcp.address, 10000);
    await mcp.execute({
      contractName: 'A',
      functionName: 'A',
      parameters: ['A', 1n],
    }).to(aliceAddress, 1000n).withoutChange().send();

    // single function MAST contract, right wrong visible unused ordering
    await fund(mcp.address, 10000);
    await mcp.execute({
      contractName: 'B',
      functionName: 'Bv',
      parameters: ['B'],
    }).to(aliceAddress, 1000n).withoutChange().send();

    // wrong visible unused ordering
    await fund(mcp.address, 10000);
    await expect(mcp.execute({
      contractName: 'C',
      functionName: 'Cv',
      parameters: ['C'],
    }).to(aliceAddress, 1000n).withoutChange().send()).rejects.toThrow();
  });

  it('test mcp with single mast contract', async () => {
    const source = `
pragma nexscript >=0.6.0;

contract Test() {

  contract A() {
    function A(string vA, int x) {
        require(vA == "A");
        require(x == 1);
    }

    function B(string vA, int x) {
      require(vA == "B");
      require(x == 2);
    }
  }
}
`;

    const mcpArtifact = compileString(source);

    const provider = new ElectrumNetworkProvider();
    const mcp = new McpContract(mcpArtifact.contracts[0], [], { provider });

    await fund(mcp.address, 10000);
    await mcp.execute({
      contractName: 'A',
      functionName: 'A',
      parameters: ['A', 1n],
    }).to(aliceAddress, 1000n).withoutChange().send();
  });

  it('test mcp with single mast contract, single function', async () => {
    const source = `
pragma nexscript >=0.6.0;

contract Test() {

  contract A() {
    function A(string vA, int x) {
        require(vA == "A");
        require(x == 1);
    }
  }
}
`;

    const mcpArtifact = compileString(source);

    const provider = new ElectrumNetworkProvider();
    const mcp = new McpContract(mcpArtifact.contracts[0], [], { provider });

    await fund(mcp.address, 10000);
    await mcp.execute({
      contractName: 'A',
      functionName: 'A',
      parameters: ['A', 1n],
    }).to(aliceAddress, 1000n).withoutChange().send();
  });

  it('test mcp with single mast contract, constraint arguments', async () => {
    const source = `
pragma nexscript >=0.6.0;

contract Test(int a, int b, int c) {

  contract A(int visible unused a, int visible unused b, int visible c) {
    function A(string vA, int x) {
        require(c == 3);

        require(vA == "A");
        require(x == 1);
    }

    function B(string vA, int x) {
      require(vA == "B");
      require(x == 2);
    }
  }
}
`;

    const mcpArtifact = compileString(source);

    const provider = new ElectrumNetworkProvider();
    const mcp = new McpContract(mcpArtifact.contracts[0], [0n, 1n, 3n], { provider });

    await fund(mcp.address, 10000);
    await mcp.execute({
      contractName: 'A',
      functionName: 'A',
      parameters: ['A', 1n],
    }).to(aliceAddress, 1000n).withoutChange().send();
  });

  it('test mcp with single mast contract, constraint arguments, single function', async () => {
    const source = `
pragma nexscript >=0.6.0;

contract Test(int a, int b, int c) {

  contract A(int visible unused a, int visible unused b, int visible c) {
    function A(string vA, int x) {
        require(c == 3);

        require(vA == "A");
        require(x == 1);
    }
  }
}
`;

    const mcpArtifact = compileString(source);

    const provider = new ElectrumNetworkProvider();
    const mcp = new McpContract(mcpArtifact.contracts[0], [0n, 1n, 3n], { provider });

    await fund(mcp.address, 10000);
    await mcp.execute({
      contractName: 'A',
      functionName: 'A',
      parameters: ['A', 1n],
    }).to(aliceAddress, 1000n).withoutChange().send();
  });

  it('test mcp with external contract references', async () => {
    const source = `
pragma nexscript >=0.6.0;

contract External(int x, int y, int visible z) {
  function dummy() {
    require(x == x);
    require(y == y);
    require(z == z);
  }
}

contract Test(int a, int b, int c) {

  contract A(int visible unused a, int visible unused b, int visible c) {
    function A(string vA, int x) {
        require(c == 3);

        require(vA == "A");
        require(x == 1);
        require(External.constraintHash.length > 0);
    }
  }
}

contract McpCovenant() {
  function CovenantConstraint() {
    require(Test.constraintHash.length > 0);
  }
}
`;

    const mcpArtifact = compileString(source);

    const provider = new ElectrumNetworkProvider();
    const mcp = new McpContract(mcpArtifact.contracts[1], [0n, 1n, 3n], {
      provider,
      dependencyArgs: {
        External: {
          constructorInputs: mcpArtifact.contracts[0].constructorInputs,
          constructorArgs: [9n, 8n, 7n],
        }
      },
    });

    await fund(mcp.address, 10000);
    await mcp.execute({
      contractName: 'A',
      functionName: 'A',
      parameters: ['A', 1n],
    }).to(aliceAddress, 1000n).withoutChange().send();
  });
});
