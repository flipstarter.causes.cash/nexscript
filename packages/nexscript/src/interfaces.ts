import { Transaction } from '@bitauth/libauth';
import { ContractCreationParams, ContractDependency } from '@nexscript/utils';
import type SignatureTemplate from './SignatureTemplate.js';
import { NetworkProvider } from './network/index.js';

export interface Utxo {
  txid: string;
  vout: number;
  satoshis: bigint;
  address: string;
  token?: TokenDetails;
}

export interface UnlockableUtxo extends Utxo {
  unlocker: Unlocker;
  options?: InputOptions;
}

export function isUnlockableUtxo(utxo: Utxo): utxo is UnlockableUtxo {
  return 'unlocker' in utxo;
}

export interface InputOptions {
  sequence?: number;
}

export interface AddInputOptions {
  transaction: any; // nexcore.Transaction
  input: Utxo; //
}

export interface SignInputOptions {
  transaction: any; // nexcore.Transaction
  inputIndex: number;
  network: string;
}

export interface Unlocker {
  // generateLockingBytecode: (options: GenerateLockingBytecodeOptions) => void;
  // generateUnlockingBytecode: (options: GenerateUnlockingBytecodeOptions) => void;

  // addOutput: (options: { transaction: any, output: any }) => void;
  addInput: (options: AddInputOptions) => void;
  signInput: (options: SignInputOptions) => void;
}

export interface UtxoP2PKT extends Utxo {
  template: SignatureTemplate;
}

export function isUtxoP2PKT(utxo: Utxo): utxo is UtxoP2PKT {
  return 'template' in utxo;
}

export interface Recipient {
  to: string;
  amount: bigint;
  token?: TokenDetails;
}

export interface Output {
  to: string | Uint8Array;
  amount: bigint;
  token?: TokenDetails;
}

export interface TokenDetails {
  amount: bigint;
  groupId: string;
}

export interface NftObject {
  groupId: string;
  commitment: string;
  amount: bigint;
}

export enum SignatureAlgorithm {
  ECDSA = 0x00,
  SCHNORR = 0x01,
}

export enum HashType {
  SIGHASH_ALL = 0x01,
  SIGHASH_NONE = 0x02,
  SIGHASH_SINGLE = 0x03,
  SIGHASH_ANYONECANPAY = 0x80,
}

// Weird setup to allow both Enum parameters, as well as literal strings
// https://stackoverflow.com/questions/51433319/typescript-constructor-accept-string-for-enum
const literal = <L extends string>(l: L): L => l;
export const Network = {
  MAINNET: literal('mainnet'),
  TESTNET3: literal('testnet3'),
  TESTNET4: literal('testnet4'),
  CHIPNET: literal('chipnet'),
  TESTNET: literal('testnet'),
  STAGING: literal('staging'),
  REGTEST: literal('regtest'),
};

export type Network = (typeof Network)[keyof typeof Network];

export interface TransactionDetails extends Transaction {
  txid: string;
  hex: string;
}

export type ContractDependencies = { [contractName: string]: ContractDependency | ContractCreationParams };

export interface ContractOptions {
  provider?: NetworkProvider,
  contractName?: string,
  dependencyArgs?: ContractDependencies,
}
